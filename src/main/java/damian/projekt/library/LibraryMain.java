package damian.projekt.library;
import java.util.Scanner;

public class LibraryMain {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Book [] ksiazki = new Book[10];
        boolean running = true;

        while (running) {

            System.out.println(
                    "1 - Dodaj książkę \n" +
                    "2 - Pokaż wykaz książek \n" +
                    "3 - Pokaż rozmiar półki \n" +
                    "0 - Wyjście");

            int command = input.nextInt();
            switch (command) {
                case 1:
                    // Dodawanie ksiązek

                    Book book = new Book();
                    System.out.println("Podaj numer na półce od 0 do "+ (ksiazki.length-1));
                    int index = input.nextInt();

                    ksiazki[index] = book;

                    System.out.println("Podaj tytuł");
                    String title = input.next();
                    book.setTitle(title);

                    System.out.println("Podaj imię autora");
                    String name = input.next();
                    book.setAuthorName(name);

                    System.out.println("Podaj nazwisko autora");
                    String surname = input.next();
                    book.setAuthorSurname(surname);

                    System.out.println("Podaj rok wydania");
                    int year = input.nextInt();
                    book.setYear(year);
                    break;

                case 2: {
                    for (Book a : ksiazki) {
                        if (a != null) {
                            a.showInfo();
                        } else {
                            System.out.println("--pusta pozycja--");
                        }
                    }
                    break;
                }
                case 3: {
                    System.out.println("Półka mieści "+ksiazki.length+" książek");
                    break;
                }
                case 0: {
                    running = false;
                    break;
                }
            }

        }
    }
}
