package damian.projekt.library;

public class Book {

    private String title;
    private String authorName;
    private String authorSurname;
    private int year;

// getters setters

    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}

    public String getAuthorName() {return authorName;}
    public void setAuthorName(String authorName) {this.authorName = authorName;}

    public String getAuthorSurname() {return authorSurname;}
    public void setAuthorSurname(String authorSurname) {this.authorSurname = authorSurname;}

    public int getYear() {return year;}
    public void setYear(int year) {this.year = year;}

// pokazanie danych książki

    public void showInfo(){
        System.out.println(this.getTitle()+" - "+this.getAuthorName()+" "+this.getAuthorSurname()+" - "+this.getYear());
    }

}
